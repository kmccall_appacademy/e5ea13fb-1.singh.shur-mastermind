class Code
  attr_reader :pegs
  PEGS = { 'r' => 'red', 'g' => 'green', 'b' => 'blue',
           'y' => 'yellow', 'o' => 'orange', 'p' => 'purple' }.freeze

  def initialize(pegs)
    @pegs = pegs.map(&:downcase)
  end

  def [](n)
    @pegs[n]
  end

  def ==(other)
    return false unless other.is_a?(Code)
    @pegs == other.pegs
  end

  def self.parse(string)
    arr = string.downcase.chars
    raise 'invalid' unless arr.all? { |c| PEGS.keys.include?(c) }
    Code.new(arr)
  end

  def exact_matches(peg_object)
    @pegs.zip(peg_object.pegs).reduce(0) do |acc, pair|
      pair.uniq.length == 1 ? acc + 1 : acc
    end
  end

  def near_matches(peg_object)
    exact = exact_matches(peg_object)
    (@pegs & peg_object.pegs).length - exact
  end


  def self.random
    colors = PEGS.keys
    arr = Array.new(4).map { |_| colors.sample }
    Code.new(arr)
  end
end

class Game
  attr_reader :secret_code

  def initialize(code = nil)
    @secret_code =
      if code.nil?
        Code.random
      else
        code
      end
    @turns_left = 10
  end

  def start
    while @turns_left > 0
      usr = get_guess
      display_matches(usr)
      if @secret_code.exact_matches(usr) == @secret_code.pegs.length
        return puts "Correct! Turns left: #{@turns_left}"
      end
      @turns_left -= 1
      puts "Turns left: #{@turns_left}"
    end
    puts 'You lose!'
  end

  def get_guess
    puts 'Guess the 4 letter combo!'
    Code.parse(gets.chomp)
  end

  def display_matches(code)
    puts "exact matches: #{secret_code.exact_matches(code)}"
    puts "near matches: #{secret_code.near_matches(code)}"
  end

end
